﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstTries;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FirstTriesTests
{
    [TestClass]
    public class PolynomialTests
    {
        [TestMethod]
        public void PositiveDeltaTest()
        {
            double a, b, c;
            a = 4;
            b = 16;
            c = 12;
            QuadraticPolynomial polynomial = new QuadraticPolynomial(a, b, c);
            List<double> results = polynomial.GetResults();
            Assert.AreEqual(2,results.Count);
            Assert.IsTrue(results.Contains(-3));
            Assert.IsTrue(results.Contains(-1));

        }
        [TestMethod]
        public void ZeroDeltaTest()
        {
            double a, b, c;
            a = 4;
            b = 16;
            c = 16;
            QuadraticPolynomial polynomial = new QuadraticPolynomial(a, b, c);
            List<double> results = polynomial.GetResults();
             Assert.AreEqual(-2, results.ElementAt(0));
        }
        [TestMethod]
        public void NegativeDeltaTest()
        {
            double a, b, c;
            a = 4;
            b = 16;
            c = 80;
            QuadraticPolynomial polynomial = new QuadraticPolynomial(a, b, c);
            List<double> results = polynomial.GetResults();
        
            Assert.AreEqual(0, results.Count);
        }
        [TestMethod]
        public void ZeroATest()
        {
            double a, b, c;
            a = 0;
            b = 16;
            c = 80;
            Assert.ThrowsException<ArgumentException>(() => new QuadraticPolynomial(a, b, c));

        }
    }
}