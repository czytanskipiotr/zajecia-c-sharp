using System;
using System.Collections.Generic;
using System.Linq;
using FirstTries;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FirstTriesTests
{
    [TestClass]
    public class OurMathTests
    {
        [TestMethod]
        public void SimpleIntegerAverage()
        {
            var numbers = new[] { 0, 5, 4, 7, 1, 2, 8, 9 };
            Assert.AreEqual(numbers.Average(), OurMath.Average(numbers));
        }
        [TestMethod]
        public void EmptyArrayAverage()
        {
            int[] numbers = new int[0];
            Assert.ThrowsException<ArgumentException>(()=>OurMath.Average(numbers));
        }
        [TestMethod]
        public void SimpleDoubleAverage()
        {
            double[] numbers = new double[] { 0.5, 0.10001, 0.2002, 0.11, 164.005512, 126.512 };
            Assert.AreEqual(numbers.Average(), OurMath.Average(numbers));
        }
       
        [TestMethod]
        public void MinMaxListTest()
        {
            var numbers = new List<double> { 0.5, 0.10001, 0.2002, 0.11, 164.005512, 126.512 };
            Tuple<double, double> results = OurMath.GetMinMax(numbers);
            Assert.AreEqual(results.Item1, numbers.Min());
            Assert.AreEqual(results.Item2, numbers.Max());
        }
        [TestMethod]
        public void MinMaxArrayTest()
        {
            var numbers = new double[] { 0.5, 0.10001, 0.2002, 0.11, 164.005512, 126.512 };
            Tuple<double, double> results = OurMath.GetMinMax(numbers);
            Assert.AreEqual(results.Item1, numbers.Min());
            Assert.AreEqual(results.Item2, numbers.Max());
        }

        [TestMethod]
        public void OmitNegatives()
        {
            var numbers = new List<int> { 0, 1, 2, -4, -6, -2, 6, -3, -7, 5, -1, -4, -2 };
            List<int> results = OurMath.OmitNegatives(numbers);
            Assert.AreEqual(results.Count, 5);
            Assert.AreEqual(results.FindAll(x => x < 0).Count, 0);
        }
        [TestMethod]
        public void CharactersStatistics()
        {
            const string message = "Ala ma kota ... tutaj jest 123 inny napis 541111 trzeba zliczyc" +
                                   "znaki, rowniez ***nie-alfanumeryczne**" +
                                   " takie jak klamerki { } i dolary $$$$";
            Dictionary<char, int> results = OurMath.GetCharactersStatistics(message);
            Assert.AreEqual(results['$'], 4);
            Assert.AreEqual(results['1'], 5);
            Assert.AreEqual(results['{'], 1);
            Assert.AreEqual(results['*'], 5);
            Assert.AreEqual(results['A'], 1);
            Assert.AreEqual(results['a'], 13);
            Assert.AreEqual(results[' '], 21);
            Assert.AreEqual(message.Length, results.Select(x => x.Value).Sum());
        }
    }
}
